import React, { useContext, useState } from "react";
import { createContext } from "react";

const ModeContext = createContext();

export const useMode = () => {
  return useContext(ModeContext);
};

export const ModeProvider = ({ children }) => {
  const [selectedOption, setSelectedOption] = useState("");

  return (
    <ModeContext.Provider value={{ selectedOption, setSelectedOption }}>
      {children}
    </ModeContext.Provider>
  );
};
