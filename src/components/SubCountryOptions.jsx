import React from "react";
import { useMode } from "../../context/ModeContext";
import { Select } from "@chakra-ui/react";

const SubCountryOptions = ({
  setSelectSubRegion,
  subRegion,
  selectsubregion,
}) => {
  const { setSelectedOption } = useMode();

  const selectHandler = (e) => {
    setSelectSubRegion(e.target.value);
    setSelectedOption("");
  };

  return (
    <Select
      cursor={"pointer"}
      fontSize={"1.2rem"}
      boxShadow={"0px 5px 10px -7px black"}
      onChange={selectHandler}
      value={selectsubregion}
      outline={"none"}
      border={"none"}
    >
      <option value="">filter by Sub Region</option>
      {subRegion.map((regionname, index) => {
        return (
          <option key={index} value={regionname}>
            {regionname}
          </option>
        );
      })}
    </Select>
  );
};

export default SubCountryOptions;
