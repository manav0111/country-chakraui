import React from "react";
import { useMode } from "../../context/ModeContext";
import { Select } from "@chakra-ui/react";

const CountryOptions = ({ regionarr, handelRegion }) => {
  const { setSelectedOption } = useMode();

  return (
    <Select
      cursor={"pointer"}
      fontSize={"1.2rem"}
      boxShadow={"0px 5px 10px -7px black"}
      outline={"none"}
      border={"none"}
      onChange={(e) => {
        setSelectedOption("");
        handelRegion(e.target.value);
      }}
    >
      <option className="region-option" value="">
        filter by Region
      </option>
      {regionarr.map((regionname, index) => {
        return (
          <option key={index} className="region-option" value={regionname}>
            {regionname}
          </option>
        );
      })}
    </Select>
  );
};

export default CountryOptions;
