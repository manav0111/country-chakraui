import {
  Container,
  Text,
  Box,
  Flex,
  Stack,
  HStack,
  Wrap,
  WrapItem,
  Center,
  AspectRatio,
} from "@chakra-ui/react";
import React, { useState, useEffect } from "react";
import SearchBox from "./SearchBox";
import CountryOptions from "./CountryOptions";

import SortCountries from "./SortCountries";
import SubCountryOptions from "./SubCountryOptions";
import Currencies from "./Currencies";

const AllCountries = () => {
  const [countries, setCountries] = useState([]);
  const [loading, setLoading] = useState(true);
  const [selectRegion, setSelectedRegion] = useState("");
  const [selectsubregion, setSelectSubRegion] = useState("");
  const [inputSearch, setInputSearch] = useState("");
  const [sortedacc, setSortAcc] = useState(null);
  const [error, setError] = useState("");
  const [currencies, setCurrencies] = useState("");

  let regionarr = countries.reduce((acc, country) => {
    if (!acc.includes(country.region)) {
      acc.push(country.region);
    }

    return acc;
  }, []);

  // Filtered Countries
  let filteredCountries = [];

  let currenciesArr = [];
  countries.forEach((country) => {
    if (country.currencies) {
      let arr = Object.values(country.currencies);

      currenciesArr = arr.reduce((acc, obj) => {
        if (!acc.includes(obj.name)) {
          acc.push(obj.name);
        }

        return acc;
      }, []);
    }

    if (
      (!selectRegion || country.region == selectRegion) &&
      (!currencies || currenciesArr.includes(currencies)) &&
      (!selectsubregion || country.subregion == selectsubregion) &&
      (!inputSearch ||
        country.name.common.toLowerCase().includes(inputSearch.toLowerCase()))
    ) {
      filteredCountries.push(country);
    }
  });

  let subRegion = countries.reduce((acc, country) => {
    if (
      country.region == selectRegion &&
      country.subregion != undefined &&
      !acc.includes(country.subregion)
    ) {
      acc.push(country.subregion);
    }

    return acc;
  }, []);

  // Sorting Logic Started
  let sortedCountries = [...filteredCountries];
  if (sortedacc == "population-asc") {
    sortedCountries.sort((country1, country2) => {
      return country1.population - country2.population;
    });
  } else if (sortedacc == "population-dsc") {
    sortedCountries.sort((country1, country2) => {
      return country2.population - country1.population;
    });
  } else if (sortedacc == "area-asc") {
    sortedCountries.sort((country1, country2) => {
      return country1.area - country2.area;
    });
  } else if (sortedacc == "area-dsc") {
    sortedCountries.sort((country1, country2) => {
      return country2.area - country1.area;
    });
  }

  filteredCountries = sortedCountries;

  const getAllCountriesData = async () => {
    try {
      let res = await fetch("https://restcountries.com/v3.1/all");
      if (!res.ok) {
        throw new Error(`HTTP error! Status: ${res.status}`);
      }
      let countryData = await res.json();
      setCountries(countryData);
      setLoading(false);
    } catch (err) {
      console.log("inside catch block");
    }
  };

  useEffect(() => {
    getAllCountriesData();
  }, []);

  console.log(filteredCountries);

  const handelSearch = (searchvalue) => {
    setInputSearch(searchvalue);
  };

  const handelSort = (sortvalue) => {
    setSortAcc(sortvalue);
  };

  const handelRegion = (regionvalue) => {
    setSelectedRegion(regionvalue);
    setSelectSubRegion("");
    setCurrencies("");
  };

  return (
    <>
      <Box width={"100vw"}>
        {!error && loading && (
          <Text fontSize={"1.5rem"} textAlign={"center"}>
            Loading...
          </Text>
        )}

        {error && (
          <Text color={"red"} fontSize={"1.2rem"}>
            {error}{" "}
          </Text>
        )}
        <Wrap margin={"auto"} width="95%" mt={16} justify="space-between">
          <Stack
            direction={["column", "row"]}
            w={"100%"}
            justify="space-between"
            gap={"2rem"}
          >
            <WrapItem>
              <Box>
                <SearchBox handelSearch={handelSearch} />
              </Box>
            </WrapItem>

            <WrapItem>
              <Box>
                <SortCountries handelSort={handelSort} />
              </Box>
            </WrapItem>

            <WrapItem>
              <Box>
                <Currencies
                  countries={countries}
                  setCurrencies={setCurrencies}
                  currenciesval={currencies}
                />
              </Box>
            </WrapItem>

            <WrapItem>
              <Box>
                <CountryOptions
                  handelRegion={handelRegion}
                  regionarr={regionarr}
                />
              </Box>
            </WrapItem>

            <WrapItem>
              <Box>
                <SubCountryOptions
                  setSelectSubRegion={setSelectSubRegion}
                  subRegion={subRegion}
                  selectsubregion={selectsubregion}
                />
              </Box>
            </WrapItem>
          </Stack>
        </Wrap>

        <Box
          margin={"auto"}
          mt={20}
          width={"95%"}
          display={"flex"}
          direction={["column", "row"]}
        >
          <Wrap justify="space-between">
            {filteredCountries.map((country) => {
              return (
                <WrapItem>
                  <Box
                    width={"24rem"}
                    boxShadow={"0px 5px 10px -7px black"}
                    my={5}
                    mx={2}
                    _hover={{ cursor: "pointer" }}
                  >
                    <AspectRatio ratio={1 / 0.6}>
                      <img
                        width={"100%"}
                        borderRadius={"5px 5px 0px 0px"}
                        src={country.flags.png}
                        alt={`${country.name.common} flag`}
                      />
                    </AspectRatio>

                    <Box p={5}>
                      <Box>
                        <Text fontSize={"1.5rem"}>{country.name.common}</Text>
                      </Box>
                      <Box fontWeight={400} opacity={"80%"} fontSize={"1.2rem"}>
                        <h4>Population: {country.population}</h4>
                        <h4>Region: {country.region}</h4>
                        <h4>Capital: {country.capital}</h4>
                        <h4>SubRegion:{country.subregion}</h4>
                      </Box>
                    </Box>
                  </Box>
                </WrapItem>
              );
            })}
          </Wrap>

          {!error && filteredCountries.length == 0 ? (
            <Text textAlign="center" fontSize={"2rem"}>
              No Countries Found
            </Text>
          ) : (
            ""
          )}
        </Box>
      </Box>
    </>
  );
};

export default AllCountries;
