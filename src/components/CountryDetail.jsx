import React, { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";
import { FaArrowLeftLong } from "react-icons/fa6";
import { useMode } from "../../context/ModeContext";
const CountryDetail = () => {
  const [countryDetail, setCountryDetail] = useState({});
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);

  const { mode, toggleMode } = useMode();

  let { id } = useParams();
  let navigate = useNavigate();

  const fetchSpecificCountry = async () => {
    try {
      let res = await fetch(`https://restcountries.com/v3.1/alpha/${id}`);
      if (!res.ok) {
        throw new Error(`error `);
      }
      let data = await res.json();

      console.log(data[0]);
      setCountryDetail(data[0]);
      setLoading(false);
    } catch (err) {
      setError(true);
    }
  };

  console.log("countryDetail:", countryDetail);

  useEffect(() => {
    fetchSpecificCountry();
  }, [id]);

  if (error) {
    return <h2 className="err">HTTP Error Occured!</h2>;
  }

  if (loading) {
    return <img className="loading" src={loader} />;
  }

  console.log(`countryDetail ${countryDetail}`);
  return (
    <>
      <div className={`detail-container ${mode == "Light" ? "light" : "dark"}`}>
        <div className="btn-container">
          <button
            onClick={() => navigate(-1)}
            className={`btn ${mode == "Light" ? "card-light" : "card-dark"}`}
          >
            <FaArrowLeftLong className="arrow" />
            Back{" "}
          </button>
        </div>

        <div className="detail">
          <img src={countryDetail.flags ? countryDetail.flags.svg : ""} />

          <div className="detail-info">
            <h1>{countryDetail.name.common}</h1>
            <ul>
              <li>
                <span>Native Name:</span>
                {countryDetail.name.nativeName
                  ? countryDetail.name.nativeName.eng
                    ? countryDetail.name.nativeName.eng.common
                    : Object.values(countryDetail.name.nativeName).slice(-1)[0]
                        .common
                  : "Not Avaliable"}
              </li>

              <li>
                <span>Population:</span>
                {countryDetail.population}{" "}
              </li>

              <li>
                <span>Region:</span>
                {countryDetail.region}{" "}
              </li>
              <li>
                <span>Sub Region:</span>
                {countryDetail.subregion
                  ? countryDetail.subregion
                  : "Not Available"}
              </li>

              <li>
                <span>Capital:</span>
                {countryDetail.capital
                  ? countryDetail.capital
                  : "Not Available"}
              </li>

              <li>
                <span>Top Level Domain:</span>
                {countryDetail.tld ? countryDetail.tld : ""}
              </li>

              <li>
                <span>Currencies:</span>
                {countryDetail.currencies
                  ? Object.values(countryDetail.currencies)
                      .reduce((acc, current) => {
                        acc.push(current.name);
                        return acc;
                      }, [])
                      .join(",")
                  : "Not Available"}
              </li>

              <li>
                <span>Languages:</span>
                {countryDetail.languages
                  ? Object.values(countryDetail.languages).join(",")
                  : "Not Available"}
              </li>
            </ul>

            <div className="border-container">
              <h4>Border Countries:</h4>
              {countryDetail.borders
                ? countryDetail.borders.map((id) => {
                    return (
                      <Link to={`/country/${id}`}>
                        <button
                          className={`btn ${
                            mode == "Light" ? "card-light" : "card-dark"
                          }`}
                        >
                          {id}
                        </button>
                      </Link>
                    );
                  })
                : "Not Available"}
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default CountryDetail;
