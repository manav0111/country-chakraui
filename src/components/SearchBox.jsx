import { Flex, Input, InputGroup, InputLeftElement } from "@chakra-ui/react";
import React, { useState } from "react";

import { AiOutlineSearch } from "react-icons/ai";

const SearchBox = ({ handelSearch }) => {
  return (
    <>
      <form>
        <InputGroup outline={"none"} border={"none"} fontSize={"1.2rem"}>
          <InputLeftElement pointerEvents="none" color="gray.300">
            <AiOutlineSearch />
          </InputLeftElement>
          <Input
            outline={"none"}
            border={"none"}
            py={6}
            px={20}
            boxShadow={"0px 5px 10px -7px black"}
            fontWeight={600}
            type="text"
            placeholder="Search for a country...."
            onChange={(e) => {
              handelSearch(e.target.value);
            }}
          />
        </InputGroup>
      </form>
    </>
  );
};

export default SearchBox;
