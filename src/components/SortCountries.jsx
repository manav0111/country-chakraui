import React, { useState } from "react";
import { useMode } from "../../context/ModeContext";
import { Select } from "@chakra-ui/react";

const SortCountries = ({ handelSort }) => {
  const { selectedOption, setSelectedOption } = useMode();

  return (
    <Select
      cursor={"pointer"}
      fontSize={"1.2rem"}
      boxShadow={"0px 5px 10px -7px black"}
      outline={"none"}
      border={"none"}
      onChange={(e) => {
        handelSort(e.target.value);
        setSelectedOption(e.target.value);
      }}
      value={selectedOption}
    >
      <option className="region-option" value="">
        Sort By
      </option>
      <option className="region-option" value="population-asc">
        Population (ASC)
      </option>
      <option className="region-option" value="population-dsc">
        Population (DSC)
      </option>
      <option className="region-option" value="area-asc">
        Area (ASC)
      </option>
      <option className="region-option" value="area-dsc">
        Area (DSC)
      </option>
    </Select>
  );
};

export default SortCountries;
