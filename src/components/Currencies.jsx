import React from "react";
import { useMode } from "../../context/ModeContext";
import { Select } from "@chakra-ui/react";

const Currencies = ({ countries, setCurrencies, currenciesval }) => {
  const { setSelectedOption } = useMode();

  const selectHandler = (e) => {
    setCurrencies(e.target.value);
    setSelectedOption("");
  };

  let currencies = countries.reduce((acc, country) => {
    if (country.currencies) {
      let currencyArr = Object.values(country.currencies);

      currencyArr.map((obj) => {
        if (!acc.includes(obj.name)) acc.push(obj.name);
      });
    }

    return acc;
  }, []);

  return (
    <Select
      cursor={"pointer"}
      fontSize={"1.2rem"}
      boxShadow={"0px 5px 10px -7px black"}
      onChange={selectHandler}
      value={currenciesval}
      outline={"none"}
      border={"none"}
    >
      <option className="region-option" value="">
        filter by Currencies
      </option>
      {currencies.map((currency, index) => {
        return (
          <option key={index} className="region-option">
            {currency}
          </option>
        );
      })}
    </Select>
  );
};

export default Currencies;
