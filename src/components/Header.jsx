import React from "react";
import { FiMoon } from "react-icons/fi";
import {
  Box,
  Flex,
  HStack,
  Spacer,
  Text,
  useColorMode,
} from "@chakra-ui/react";

const Header = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <>
      <Box width={"100vw"} boxShadow={"0px 5px 10px -7px black"} p={8}>
        <HStack>
          <Text fontSize={"2rem"} fontWeight={500}>
            {" "}
            Where in the World?
          </Text>
          <Spacer />
          <Box _hover={{ cursor: "pointer" }} onClick={toggleColorMode}>
            <Flex gap={5}>
              <FiMoon size="24" />
              <p >
                <Text fontSize={"1.3rem"}>
                  {colorMode == "light" ? "Dark" : "light"} Mode
                </Text>
              </p>
            </Flex>
          </Box>
        </HStack>
      </Box>
    </>
  );
};

export default Header;
