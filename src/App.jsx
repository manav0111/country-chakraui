import { useState } from "react";
import reactLogo from "./assets/react.svg";
import viteLogo from "/vite.svg";
import { Outlet } from "react-router-dom";
import Header from "./components/Header";
import { ModeProvider } from "../context/ModeContext";
import { Box, border } from "@chakra-ui/react";

function App() {
  return (
    <Box boxSizing={"border-box "}>
      <ModeProvider>
        <Header />
        <Outlet />
      </ModeProvider>
    </Box>
  );
}

export default App;
